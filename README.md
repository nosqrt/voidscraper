# VoidScaper
## Finds the latest kernels from the Void Linux repository and creates corresponding ebuilds.

### Dependencies
This project only needs one dependency. This is XML::Simple.

On Gentoo you can install this with
`emerge -av XML-Simple`

### Script usage
You can get a copy of this script with:
`git clone https://gitlab.com/nosqrt/voidscraper.git`

The script needs one argument. This is the path to the ebuild directory you are going to place the
void-sources-bin and void-sources-headers-bin ebuilds into.

Example usage:
`./voidscraper.pl /overlay/sys-kernel`

