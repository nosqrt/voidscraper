#!/usr/bin/env perl

use strict;
use warnings;
use File::Fetch;
use File::Path;
use File::Copy qw(copy);
use XML::Simple;

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# This script aims to automatically update the void linux kernel ebuild.                #
#                                                                                       #
# Arguments:                                                                            #
#   ARGV[0] -> Base path of the repo you want to edit.                                  #
#   ARGV[1] -> Distfiles download location.                                             #
#                                                                                       #
# Usage Example:                                                                        #
#   voidscaper.pl /overlay/sys-kernel /var/cache/repos/gentoo                           #
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

sub MakeConnection
{
    ## Returns a hash with the contents of the page, provided the HTTP return code is 'OK'.
    my $url = File::Fetch->new( uri => "$_[0]" );
    my $fetched = $url->fetch or die $url->error;

    # Create a directory where we'll unpack the gzip file.
    mkdir("voidList");
    system("tar xzf $fetched -C voidList");
    my $hash = XMLin("voidList/index.plist");

    # Cleanup CWD.
    rmtree("voidList");
    unlink($fetched);

    return $hash;
}

sub SortLinux
{
    ## Returns an array containing the package which are either kernel headers or kernels.
    my @array;
    my $dataHash  = $_[0];
    my @dataArray = @{$_[0]{dict}{key}};

    # Find all of the linux builds.
    my @linux = grep { /^linux\d.\d(.\d)?.*/ } @dataArray;

    # Get the corresponding array positions for the linux builds.
    for (my $i=0; $i <= $#dataArray; $i++) {
        foreach (@linux) { push(@array, $i) if $_ eq $dataArray[$i] };
    }

    # Get the full voidlinux name for each linux build.
    map { $_ =~ s/linux//ig ; $_ } map { $dataHash->{dict}{dict}[$_]{string}->[6] } @array;
}

sub FilterLinux
{
    ## Returns a hash which has sorted the kernel/headers. E.G.
    #  {
    #      'kernel' => { '4.8' => { '4.8.1_1' } },
    #      'header' => { '4.8' => { '4.8.1_1' } }
    ## }
    my @array = @_;
    my %filter;

    foreach my $i (@array)
    {
        # Use a matching regex to tokenize the package and assign to human-readable names.
        $i =~ m/([\d|\.]+)(-(\w+))?-([\d|\.]+_?\d+?)/;
        my ($name, $slot, $version) = ($1, (defined($3) ? $3 : 'kernel'), $4);

        # Avoids overwriting an existing hash key. Init the new key with an empty array if none exists.
        (! grep { $_ eq $version } keys(%{$filter{$slot}{$name}})) and $filter{$slot}{$name} = $version;
    }

    return \%filter;
}

sub UpdateEbuild($$$$)
{
    my ($ebuild, $name, $type, $linux) = @_;

    # Get the ebuild friend version number of the kernel.
    my @kernels = sort { $a cmp $b } map { /([\d|.]+)_1/; $1 } values %{$linux->{$name}};

    # Delete everything in the ebuild directory.
    foreach (glob("$ebuild/*")) { unlink($_) };

    # Copy the templates with the appropriate name.
    foreach (@kernels) { copy ("template-$type.ebuild", "$ebuild/void-sources-$type-$_.ebuild") };
    copy ("metadata-$type.xml", "$ebuild/metadata.xml");

    return sub()
    {
        my $dist = shift;
        my @command = (
            "repoman manifest",
            "git add -- .",
            "git commit . -S -m 'Automatic update for $name\n$name updated to @kernels'"
        );

        # Change into the ebuild directory, set DISTDIR so we don't need root and run ebuild commands.
        chdir($ebuild);
        $ENV{DISTDIR} = "$dist";
        foreach (@command) { system("$_"); };
    }
}

sub GitPush($$)
{
    chdir($_[0]);
    system("repoman full");
    system("git push origin $_[1]");
}

# Setup links and paths needed to run the script.
my $voidlink = 'http://mirror.aarnet.edu.au/pub/voidlinux/current/x86_64-repodata';
my $home     = "$ARGV[0]";
my $dist     = "$ARGV[1]";
my $headers  = "$home/void-sources-headers-bin";
my $bin      = "$home/void-sources-bin";

# Generate the needed linux hash.
my $linux = FilterLinux(SortLinux(MakeConnection($voidlink)));

# Setup the closures, the first part removes and adds relevant ebuilds to their correct dir.
my $headerUpdate = UpdateEbuild($headers, 'headers', 'headers-bin', $linux);
my $binUpdate    = UpdateEbuild($bin, 'kernel', 'bin', $linux);
$headerUpdate->($dist);
$binUpdate->($dist);

GitPush($home, 'master');
